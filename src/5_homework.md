---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.10.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# 5.3. Homework

## Exercise 1: Cat states

Consider two <a href=https://en.wikipedia.org/wiki/Cat_state>Shroedinger cat states</a> defined by:

$$
|{\rm cat1}\rangle = A \left(|\alpha\rangle + |-\alpha\rangle\right)
$$

$$
|{\rm cat2}\rangle = A \left(|\alpha\rangle - |-\alpha\rangle\right)
$$

where $|\alpha\rangle$ is a coherent state with complex amplitude $\alpha$ and $A$ is a normalisation constant (which is not important now and which you can look up on wikipedia if you like...).  

**(a)** Using the mathematical definition of the coherent state, find the coefficients $c_n = \langle \psi | n \rangle$ of the two cat states. Do not solve for the normalization constant. What do you notice about the photon number distributions of the two cats? 

**(b)** Confirm your result in (a) by creating an $\alpha = 3$ cat state in QuTiP and plotting the Fock (number) distribution. 

**(c)** Make side-by-side plots of the Wigner functions of the two cats in QuTiP. What is different in the Wigner function of the two cats? 

**(d)** A curious property of the coherent state is that if you operate on it with the annihilation operator $\hat a$, and then renormalize the wave function afterwards, you get the same state back. As we will see later, and as maybe makes a bit of sense intuitively, "operating with the annihilation operator" can be interpreted as a measurement of a photon emitted by the quantum state. What happens to the state $|{\rm cat1}\rangle$ if it emits a photon? 

*(In <a href=https://en.wikipedia.org/wiki/Quantum_error_correction#Bosonic_codes>bosonic quantum computing</a>, your answer from (d) are referred to as "parity jumps", for reasons you can probably figure out. If you are interesting in quantum computing with circuit QED, <a href=http://qs3.mit.edu/images/pdf/QS3-2017---Girvin-Lecture-2.pdf>this lecture</a> by Steve Girvin from Yale gives a nice intro to computing with cat states at a level you should be able to follow at least partially)*


## Exercise 2: A funny Wigner function

In his research group meeting, Gary proposes that his team try to create a new type of exotic quantum state with the following Wigner function:

$$
W(x,p) = Axe^{-x^2/l^2}e^{-l^2 p^2 / \hbar^2}
$$

where $A$ is a normalization constant and $l^2 = 2x_{zpf}^2$. 

**(a)** Make a sketch of the function, either by hand or using python's `matplotlib` library.

**(b)** What do you think of Gary's proposal? If you were his PhD student, would you follow his advice? Why / why  not? 

