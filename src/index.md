# Quantum Sensing and Measurement

Welcome to the Lecture Notes of the Quantum Sensing and Measurement course. As we delve into the intricate world of quantum systems, we will explore the interplay between classical and quantum noise, the nuances of quantum measurement, and the dynamics of open and monitored quantum systems. 

!!! live-code "Jupyter notebook mode" 
    Look for :fontawesome-solid-wand-magic-sparkles: icon in the title of a lecture note and click it to turn lecture notes into an interactive Jupyter notebook!

!!! pull "Prerequisites"
    - Before embarking on this journey, students should possess a foundational understanding of Mathematics for Quantum Physics, Quantum Physics, Information, and Computation. 
    
    - Familiarity with the quantization of the harmonic oscillator, especially its operator formulation, is crucial. 
    
    - Additionally, a good grasp of the basic postulates of quantum mechanics, quantum measurement, and the formalism for predicting the outcomes of quantum measurements using the generalized statistical interpretation is essential (at the level, for example, of chapter 3 of Griffiths).

!!! summary "**Course Overview:**"

    1. **Foundations:** We'll start by revisiting classical concepts like sensitivity, noise, and power spectral density.

    2. **Harmonic Oscillator:** A comprehensive review of the harmonic oscillator will be undertaken, emphasizing the Harmonic oscillator coherent state, its properties, the quadrature representation of classical 
    and quantum coherent fields, and Wigner functions.

    3. **Quantum Noise:** Using the harmonic oscillator, we'll introduce the concept of quantum (photon) shot noise in coherent states.

    4. **Continuous Measurement:** Understand the paradigm of continuous measurement, starting from the generalised statistical interpretation and progressing to the Monte Carlo wavefunction technique.

    5. **Noise Limits:** Delve into the shot noise limit for imprecision noise and the radiation pressure shot noise limit in optomechanics. We'll also explore the standard quantum limit (SQL) of sensing and measurement.

    6. **Density Matrix:** Introducing the density matrix as a pivotal tool for understanding quantum and classical noise in qubits and quantum optics.

    7. **Mathematical Formalization:** We'll formalize the mathematics of quantum noise using the density matrix and the Lindblad master equation.

    8. **QuTiP Software Package:** Get hands-on with the QuTiP software package, a powerful tool for simulating the dynamics of open and monitored quantum systems.

    9. **Quantum Dynamics with QuTiP:** Witness basic examples of quantum dynamics illustrated using QuTiP.

    10. **Simulation Project:** In the concluding segment, students will have the opportunity to simulate a quantum dynamics process, integrating noise and quantum measurement in a quantum system of their choice, with guidance from our dedicated staff.

!!! abstract "Learning materials"
    With these notes, our aim is to provide learning materials which are:
    - self-contained
    - interactive with live code
    - easy to modify and remix, so we provide the [full source](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement)
    - open for reuse: see the [license](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement/-/blob/master/LICENSE.md).

!!! question "Contributions"
    Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement), especially do [let us know](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement/issues/new?issuable_template=typo) if you see a typo!
