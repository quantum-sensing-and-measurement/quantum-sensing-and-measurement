---
jupyter:
  jupytext:
    formats: md,ipynb
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Lecture 5.1: The Wigner Quasi-Probability Distribution

!!! pull "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    1. Analyze the properties of the coherent state wavefunction   
    2. Recall that $\rho(x) = \Psi(x)^*\Psi(x)$ describes a probability distribution

!!! goals "Learning goals"

    After this lecture you will be able to:

    1. Describe the main properties of the Wigner function
    2. Describe the relation between the Wigner function and the probability distribution of a quantum state 
    
---

In the last lecture we discussed some properties of quantum fluctuations of the ground state of the harmonic oscillator, and took a look at an interesting state of the quantum harmonic oscillator known as the coherent state.

We will now inspect the coherent state in more detail, but first we will study a different way of representing the wavefunction called the Wigner quasiprobability distribution, or for short, the Wigner function.  

1. For a single particle in one dimension in quantum state with wavefunction $\psi(x)$, the Wigner function is defined as 
    $$
    W(x,p) = \frac{1}{\pi \hbar} \int_{-\infty}^{+\infty} \psi^*(x+y) \psi(x-y) e^{2ipy/\hbar} dy
    $$
    where $p$ is just the 1D scalar momentum, not the momentum operator.  

2. We can also form $W(x,p)$ with $\phi(p)$, the Fourier transform of $\psi(x)$:
    $$
    W(x,p) = \frac{1}{\pi \hbar} \int_{-\infty}^{+\infty} \phi^*(p+q) \phi(p-q) e^{-2iqx/\hbar} dq
    $$

How should we interpret the Wigner function?  

It must be something like a probability distribution, as the name suggests.  

1. If we integrate $W(x,p)$ over all $p$, we get $|\psi(x)|^2$:
    $$
    \int W(x,p) dp = |\psi(x)|^2
    $$
    which *is* a probability distribution.  

2. Similarly, if we integrate over all $x$, we get $|\phi(p)|^2$.
    $$
    \int W(x,p) dx = |\phi(p)|^2 
    $$

So it's close, but there are two problems with calling $W(x,p)$ a probability distribution:

- In quantum mechanics, we can only measure one thing at a time, and after measuring our wave function, it collapses, so $W(x,p)$ *cannot* be the probability of measuring both $x$ and $p$, which are non-commuting observables

- for some interesting quantum states, $W(x,p)$ can be negative, and since probabilities cannot be negative, we must call it a quasiprobability distribution instead.

!!! note ""

    Note that although $W(x,p)$ can be negative, it must be constructed such that its integrals over $x$ and $p$ are positive, since these do indeed represent probability 
    distributions, as shown above.

    Also note that the Wigner function is real-valued, though it may not be obvious from the formula above.

To make this more concrete, let's calculate the Wigner function of the ground state of the harmonic oscillator:

$$
\psi_0(x) = \bigg( \frac{m\omega}{\pi\hbar} \bigg)^{\frac{1}{4}} e^{\frac{-m\omega x^2}{2\hbar}}
$$

1. Defining $l^2 = \frac{\hbar}{m\omega} = 2x_{ZPF}^2$, we can write
    $$
    \psi_0(x) = \bigg( \frac{1}{\pi l^2} \bigg)^{\frac{1}{4}} e^{\frac{-x^2}{2 l^2}}
    $$

2. Plugging into the Wigner function, we get
    $$
    \begin{align}
    W(x,p) &= \frac{1}{\pi \hbar} \int e^{2ipy/\hbar} \psi^*(x+y) \psi(x-y) dy\\
    &= \frac{1}{\pi \hbar} \bigg( \frac{1}{\pi l^2} \bigg)^{\frac{1}{2}} \int e^{2ipy/\hbar} e^{-(x+y)^2/2l^2} e^{-(x-y)^2/2l^2} dy
    \end{align}
    $$

3. After working out the exponents, we get
    $$
    W(x,p) = \frac{1}{\pi \hbar} \bigg( \frac{1}{\pi l^2} \bigg)^{\frac{1}{2}} e^{-x^2 / l^2} \int e^{2ipy/\hbar} e^{-y^2 / l^2} dy
    $$
    which is just the Fourier transform of a Gaussian, which is still a Gaussian.  

4. Thus, we have
    $$
    W(x,p) = \frac{1}{\pi \hbar} e^{-x^2 / l^2} e^{-l^2 p^2 / \hbar^2}
    $$
    This is a two-dimensional Gaussian "disk" centered around $x,p=0$;
    <figure markdown>
    ![](figures/5/wigner_plane.PNG){: style="width:300px; display: block; margin-left: auto; margin-right: auto;"}
    <figcaption> </figcaption>
    </figure>
    

In the next two sections of this lecture, we will explore the Wigner functions of different quantum states using a Python library called QuTiP.  

The homework for this lecture is at the bottom of the last section ("Wigner Functions of Mixed States").



