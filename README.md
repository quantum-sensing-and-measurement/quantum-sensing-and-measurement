# Quantum Sensing and Measurement

Lecture notes and teaching material used for the Delft University of Technology course on Quantum Sensing and Measurement.

The compiled materials are available at https://qsm.quantumtinkerer.tudelft.nl

# Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.
