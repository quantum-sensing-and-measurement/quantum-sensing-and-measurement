# Search index file cleaner. 
# It removes the source code of the matplotlib's animations from the search index file to unbreak the website search in the mkdocs material template 
import json
import re
searchindex = open('site/search/search_index.json', 'r')
dictionary = json.load(searchindex)
searchindex.close()

searchindex = open('site/search/search_index.json', 'w')
for i in range(len(dictionary['docs'])): 
    
    input = dictionary['docs'][i]['text']
    output = re.sub('(function Animation).*setTimeout', 'ABBAABBA', str(input))
    dictionary['docs'][i]['text'] = output

json.dump(dictionary, searchindex, ensure_ascii=False)
searchindex.close()
