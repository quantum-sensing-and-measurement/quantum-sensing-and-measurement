{% extends 'markdown/index.md.j2' %}

{% block input %}
{% if cell.metadata.attributes and cell.metadata.attributes.initialize %}
<div class="thebelab-init-code">
<pre class="thebelab-code" data-executable="true" data-language="python">
{{cell.source}}
</pre>
</div>
{% elif cell.metadata.attributes and cell.metadata.attributes.editable == 'true' %}
<pre data-executable="true" data-language="python">{{cell.source}}</pre>
{% endif %}
{% endblock input %}


{% block display_data %}
{% if cell.metadata.attributes and cell.metadata.attributes.editable == 'false' and not cell.metadata.attributes.initialize %}
{{ super() }}
{% elif cell.metadata.attributes and cell.metadata.attributes.initialize %}
{% if output.data['text/html'] %}
<div class="jupyter output text/html">
{{ output.data['text/html'] }}
</div>
{% elif cell.metadata.attributes and cell.metadata.attributes.interact %}
<pre class="thebelab-code" data-executable="true" data-language="python">{{cell.source}}</pre>
<div class="thebelab-output" data-output="false" markdown="1">
</div>
{% endif %}
{% else %}
<pre class="thebelab-code" data-executable="true" data-language="python">{{cell.source}}</pre>
<div class="thebelab-output" data-output="true" markdown="1">
{% if output.data['text/html'] %}
<div class="jupyter output text/html">
{{ output.data['text/html'] }}
</div>
{% else %}
{{ super() }}
{% endif %}
</div>
{% endif %}
{% endblock display_data %}

{% block error %}
{% endblock error %}

{% block stream %}
{%- if output.name == 'stdout' -%}
{{ output.text | indent }}
{%- endif -%}
{% endblock stream %}